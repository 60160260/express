const express = require('express')
const app = express()

app.get('/', (req, res, next) => {
  res.json({ message: 'Hello Yosapron' })
})

app.listen(9001, () => {
  console.log('Application is runningon port 9001')
})
